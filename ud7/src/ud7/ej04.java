package ud7;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class ej04 {

	public static void main(String[] args) {
		/*
		4) Combina los m�todos generados en las actividades 2 y 3 creando una aplicaci�n que gestione ventas y control de stock en una misma interfaz. 
		Utiliza para ello las estructuras de datos que creas conveniente.
		*/
		Scanner scan = new Scanner(System.in);

		//base de datos de 10 articulos
		Hashtable<String, String> articulos = new Hashtable<String, String>();
		
		//diccionario de datos articulo:precio
		articulos.put("Aceitunas","1.50");
		articulos.put("Alcachofa","1");
		articulos.put("Cereales","1.10");
		articulos.put("Cerveza","0.24");
		articulos.put("Fuet","2");
		articulos.put("Leche","1");
		articulos.put("Patatas","0.89");
		articulos.put("Pizza","2.50");
		articulos.put("Pollo","5.25");
		articulos.put("Platanos","1.89");		
		
		//podr� a�adir articulos nuevos y cantidad
		
		int opcion;
		boolean salir = false;
		final double IVA = 0.21;
		double cantidad=0;
		double total=0, totaliva=0, precio=0, pagado=0, cambio=0;
		
			while (!salir) {
				System.out.println("1 - Venta productos");
				System.out.println("2 - Mostrar la base de datos");
				System.out.println("3 - A�adir productos");
				System.out.println("4 - Salir");
				
				System.out.println("Que quiere hacer: ");
				opcion = scan.nextInt();
				
				
				switch(opcion){
	            case 1:
	                System.out.println("Venta productos:");

	        		ArrayList<Double> nota = new ArrayList<>();
	        		
	        		System.out.println("Cantidad de productos a cobrar: ");
	        		cantidad = scan.nextInt();
	        		
	        		for (int i = 0; i < cantidad; i++) {
	        			System.out.println("Precio del producto: ");
	        			precio = scan.nextDouble();
	        			
	        			nota.add(precio);
	        			total = total + precio;
	        		}
	        		
	        		//IVA Aplicado (21 % o 4%)
	        		System.out.println("El IVA aplicado es: " + IVA);
	        		
	        		totaliva = (total + (total * IVA));
	        		//Precio total bruto y precio mas IVA
	        		System.out.println("El total sin IVA es: " + total);
	        		System.out.println("El total con IVA es: " + totaliva);
	        		// Numero de productos
	        		System.out.println("Cantidad del productos: " + cantidad);
	        		//Cantidad pagada
	        		System.out.println("Cantidad que el cliente paga: ");
	        		pagado = scan.nextDouble();
	        		cambio = pagado - totaliva;
	        		//Cambio a devolver
	        		System.out.println("Cambio a devolver: " + cambio);
	        		
	                	
	                break;
	            case 2:
	            	System.out.println("Productos de la base de datos:");
                	
                	Enumeration<String> enumeration = articulos.elements();
                	Enumeration<String> llaves = articulos.keys();
                	
                	while(enumeration.hasMoreElements()) {
                		System.out.println("Articulo y precio: " + llaves.nextElement() + " " + enumeration.nextElement() + "� ");
                	}
                	
                	break;
	             case 3:
	            	 	System.out.println("A�adir producto a la base de datos");
	                	
	                	System.out.println("Nombre");
	                	String name = scan.next();
	                	
	                	System.out.println("Precio");
	                	String precios = scan.next();
	                	
	                	articulos.put(name, precios);
	                
	                break;
	             case 4:
	            	 System.out.println("Que vaya bien!");
		                salir=true;
		                break;
	             default:
	                System.out.println("Solo n�meros entre 1 y 3");
				
			}
		
		}
			scan.close();
	}
}
