package ud7;

import java.util.Hashtable;
import java.util.Scanner;

public class ej01 {

	public static void main(String[] args) {
		/*
		1) Crea una aplicación que calcule la nota media de los alumnos pertenecientes al curso de programación. 
		Una vez	calculada la nota media se guardara esta información en un diccionario de datos que almacene la nota media de cada alumno. 
		Todos estos datos se han de proporcionar por pantalla.
		*/

		double m1 = 0, m2 = 0, m3 = 0;
		
		double media[] = new double [3];
		
		media = notaMedia(media, m1, m2, m3);
		
		mostrar(media);
		
		Hashtable<String,Double> alumnos = new Hashtable<String, Double>();
		
		alumnos.put("alumno 1", media[0]);
		alumnos.put("alumno 2", media[1]);
		alumnos.put("alumno 3", media[2]);
		
	}
	
	public static double[] notaMedia(double media[], double m1, double m2, double m3) {
		Scanner scan = new Scanner(System.in);
		
		for (int i = 0; i < media.length; i++) {
			System.out.println("Nota m1 alumno " + i);
			m1 = scan.nextDouble();
			
			System.out.println("Nota m2 alumno " + i);
			m2 = scan.nextDouble();
			
			System.out.println("Nota m3 alumno " + i);
			m3 = scan.nextDouble();
			
			double mediana = ((m1 + m2 + m3) / 3);
			
			media[i] = mediana;
			
		}
		scan.close();
		return media;
	}
	
	public static  void mostrar(double media[]) {
		
		for (int i = 0; i < media.length; i++ ) {
			
			System.out.println(media[i]);
			
		}
		
	}

}
