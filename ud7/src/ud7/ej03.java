package ud7;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class ej03 {

	public static void main(String[] args) {
		/*
		3) Crea una base de datos de 10 art�culos para controlar el stock de productos de una tienda por medio de un diccionario de datos (articulo:precio). 
		El usuario podr� a�adir, por medio de interfaz visual art�culos nuevos y cantidades de estos. 
		El usario podr� consultar la informaci�n almacenada en el diccionario referente a un articulo concreto e 
		incluso listar toda la informaci�n en la terminal del programa.
		*/
		Scanner scan = new Scanner(System.in);

		//base de datos de 10 articulos
		Hashtable<String, String> articulos = new Hashtable<String, String>();
		
		//diccionario de datos articulo:precio
		articulos.put("Aceitunas","1.50");
		articulos.put("Alcachofa","1");
		articulos.put("Cereales","1.10");
		articulos.put("Cerveza","0.24");
		articulos.put("Fuet","2");
		articulos.put("Leche","1");
		articulos.put("Patatas","0.89");
		articulos.put("Pizza","2.50");
		articulos.put("Pollo","5.25");
		articulos.put("Platanos","1.89");		
		
		//podr� a�adir articulos nuevos y cantidad
		
		int opcion;
		boolean salir = false;
		
			while (!salir) {
				System.out.println("1 - consultar la base de datos");
				System.out.println("2 - a�adir productos");
				System.out.println("3 - Salir");
				
				System.out.println("Que quiere hacer: ");
				opcion = scan.nextInt();
				
				
				switch(opcion){
	            case 1:
	                System.out.println("Productos de la base de datos:");
	                	
	                	Enumeration<String> enumeration = articulos.elements();
	                	Enumeration<String> llaves = articulos.keys();
	                	
	                	while(enumeration.hasMoreElements()) {
	                		System.out.println("Articulo y precio: " + llaves.nextElement() + " " + enumeration.nextElement() + "� ");
	                	}
	                	
	                break;
	            case 2:
	                System.out.println("A�adir producto a la base de datos");
	                	
	                	System.out.println("Nombre");
	                	String name = scan.next();
	                	
	                	System.out.println("Precio");
	                	String precio = scan.next();
	                	
	                	articulos.put(name, precio);
	                
	                break;
	             case 3:
	                System.out.println("Que vaya bien!");
	                salir=true;
	                break;
	             default:
	                System.out.println("Solo n�meros entre 1 y 3");
				
			}
		
		}
			scan.close();
	}
}
