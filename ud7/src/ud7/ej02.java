package ud7;

import java.util.ArrayList;
import java.util.Scanner;

public class ej02 {

	public static void main(String[] args) {
		/*
		2) Crea una aplicación que gestione el flujo de ventas de una caja de supermercado. 
		El programa guardara la cantidades del carrito de compra dentro de una lista. 
		Mostrará por pantalla la siguiente informacion:
		
		• IVA aplicado (21% o 4%)
		• precio total bruto y precio mas IVA.
		• Numero de artículos comprados.
		• Cantidad pagada.
		• Cambio a devolver al cliente.
		*/

		Scanner scan = new Scanner(System.in);
		
		//Variables
		final double IVA = 0.21;
		int cantidad=0;
		double total=0, totaliva=0, precio=0, pagado=0, cambio=0;

		
		ArrayList<Double> nota = new ArrayList<>();
		
		System.out.println("Cantidad de productos a cobrar: ");
		cantidad = scan.nextInt();
		
		for (int i = 0; i < cantidad; i++) {
			System.out.println("Precio del producto: ");
			precio = scan.nextDouble();
			
			nota.add(precio);
			total = total + precio;
		}
		
		//IVA Aplicado (21 % o 4%)
		System.out.println("El IVA aplicado es: " + IVA);
		
		totaliva = (total + (total * IVA));
		//Precio total bruto y precio mas IVA
		System.out.println("El total sin IVA es: " + total);
		System.out.println("El total con IVA es: " + totaliva);
		// Numero de productos
		System.out.println("Cantidad del productos: " + cantidad);
		//Cantidad pagada
		System.out.println("Cantidad que el cliente paga: ");
		pagado = scan.nextDouble();
		cambio = pagado - totaliva;
		//Cambio a devolver
		System.out.println("Cambio a devolver: " + cambio);
		
		scan.close();
	}
	
	

}
